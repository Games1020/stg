// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CodingConventionsActor.generated.h"

/** Enumの宣言方法 */
UENUM()
enum class ECodingConventions : uint8
{
	None,
	Hoge,
	Fuga
};

/** 
* Classの宣言
* AActor なので 頭文字が A
*/
UCLASS()
class ACodingConventionsActor : public AActor
{
	GENERATED_BODY()

	/**
	* Set関数 
	* 引数は参照にするようにしてください。
	*/
	void SetIntHoge(const int32& InHoge)
	{
		IntHoge = InHoge;
	}

	/**
	* Get関数
	* なるべく Const をつけて渡すようにしてください。
	*/

	/** int32型 */
	int32 GetIntHoge() const
	{
		return IntHoge;
	}
	/** bool型 */
	bool IsHoge() const
	{
		return bIsHoge;
	}

private:
	/** int型 */
	int32 IntHoge;
	/** float型 */
	float FloatHoge;
	/** bool型 */
	bool bIsHoge;
	/** Enum型 */
	ECodingConventions Template;

	/**
	* 以下 文字列
	*/

	/** 
	* FName型
	* 一番軽いので 推奨
	*/
	FName NameHoge;

	/** 
	* FText型
	* 一番使ってはいけない変数
	* 運営もなぜか 利用は非推奨
	*/
	FText TextHoge;

	/**
	* FString型
	* 一番汎用的に使われる変数
	*/
	FString StringHoge;
};

/**
* Classの宣言
* UObject なので 頭文字が U
*/
UCLASS()
class UCodingConventionsObject : public UObject
{
	GENERATED_BODY()
};

/** 
* 構造体 
* 頭の頭文字が F になる
*/
USTRUCT()
struct FCodingConventionsStruct
{
	GENERATED_BODY()
};
